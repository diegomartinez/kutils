
---
-- Base class for all objects.
--
-- @classmod class.Object

-- NOTE: This file is only for documentation purposes.
-- The actual implementation is in `init.lua`.

---
-- Object constructor.
--
-- @function Object:init

---
-- Convert the object to a string representation.
--
-- @function Object:tostring

return require "class".Object


---
-- Class of class objects.
--
-- **Extends:** `class.Object`
--
-- @classmod class.Class

-- NOTE: This file is only for documentation purposes.
-- The actual implementation is in `init.lua`.

return require "class".Class

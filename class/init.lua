
---
-- Simple class system.
--
-- Class are created with the `new` function. Once a class is created,
-- you may call it directly (or use the `newinstance` function) to
-- create an instance of the class.
--
-- Note:
-- Due to metatables, calling the module results in a call to the `new`
-- function. The following two lines are equivalent:
--
--     local class = require "class"
--     MyClass = class("mymod.MyClass")
--     MyClass = class.new("mymod.MyClass")
--
-- @module class

local M = { }

local function new(name, super)
	super = super or M.Object
	local mt = { }

	mt.__name = name
	mt.__super = super
	mt.__index = super
	mt.__call = M.newinstance
	mt.__class = M.Class
	function mt:__tostring()
		return "<class "..name..">"
	end

	local cls = { }
	cls.__name = name
	cls.__index = cls
	cls.__super = super
	cls.__class = cls
	cls.__tostring = function(self)
		return self:tostring()
	end
	return setmetatable(cls, mt)
end

---
-- Create a new class.
--
-- @tparam string name  Fully-qualified class name.
-- @tparam Class super  Parent class this class inherits from.
-- @return A `Class`.
function M.new(name, super)
	return new(name, super)
end

function M.newinstance(cls, ...)
	local inst = setmetatable({ }, cls)
	inst:init(...)
	return inst
end

local Object = new("class.Object")

---
-- Base class for all classes.
M.Object = Object

---
-- Object constructor.
function Object:init()
end

---
-- Convert the object to a string representation.
function Object:tostring()
	return "<"..M.type(self)..">"
end

local Class = new("class.Class")

---
-- Class of class objects.
M.Class = Class

getmetatable(Object).__class = M.Class

---
-- Get the class of an object.
--
-- @tparam any x  Object to check.
-- @return A `Class`, or nil if this object does not have a class as
--  defined by this module.
function M.class(x)
	local mt = getmetatable(x)
	return mt and mt.__class
end

---
-- Get the superclass of an object.
--
-- @tparam any x  Object to check.
-- @return A `Class`, or nil if this object does not have a class as
--  defined by this module.
function M.super(x)
	local mt = getmetatable(x)
	return mt and mt.__super
end

---
-- Check if an object is an instance of a class.
--
-- @tparam any x  Object to check.
-- @tparam Class cls  Base class to check.
-- @return True if `x` is an instance of `cls`, false if not, or
--  nil if its underlying type is not a table.
function M.isinstance(x, cls)
	if type(x) ~= "table" then
		return nil
	end
	cls = cls or Object
	x = M.class(x)
	while x do
		if rawequal(x, cls) then
			return true
		end
		x = M.super(x)
	end
	return false
end

---
-- Get the type of an object as a string.
--
-- If the object is an instance of some class as defined by this
-- module, returns its type name. Otherwise, acts the same as the
-- built-in `type` function.
--
-- @tparam any x  Object.
-- @return A string.
function M.type(x)
	local cls = M.class(x)
	return cls and cls.__name or type(x)
end

---
-- Utility function to bind a method to an object.
--
-- @tparam function func  Method.
-- @tparam any inst  Instance.
-- @return A function.
function M.bind(func, inst)
	return function(...)
		return func(inst, ...)
	end
end

local ok, expect = pcall(require, "expect")

if ok then -- Self-tests.

	local C1 = M.new("C1")
	function C1:init(x)
		self.x = x
	end
	C1.x = 1
	local C2 = M.new("C2", C1)
	C2.x = 2
	local i1, i2, i3 = C1(), C2(), C2(3)

	expect(1, i1.x)
	expect(2, i2.x)
	expect(3, i3.x)
	expect(true, M.isinstance(i1, C1))
	expect(true, M.isinstance(i2, C1))
	expect(false, M.isinstance(i1, C2))
	expect(true, M.isinstance(i2, C2))
	expect(nil, M.isinstance(1, M.Object))
	expect("class.Object", M.type(M.Object()))
	expect("C1", M.type(i1))
	expect("C2", M.type(i2))
	expect("C2", M.type(i3))
	expect("class.Class", M.type(C1))
	expect("table", M.type({ }))

end

return setmetatable(M, {
	__call = function(_, ...)
		return M.new(...)
	end,
})

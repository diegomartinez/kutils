
---
-- Extra string utilities.
--
-- @module stringex

local M = { }

local strfind, strsub, strmatch = string.find, string.sub, string.match

---
-- Split a string.
--
-- @tparam string str  String to split.
-- @tparam string sep  Separator string or pattern.
-- @tparam ?boolean ispat  If true, `sep` is treated as a Lua pattern.
--  If false, it's treated as a plain string. Default is false.
-- @treturn function A function yielding the next part or nil if there
--  are no more parts.
function M.isplit(str, sep, ispat, filter)
	local pos, endp = 1, #str+1
	local isplain = not ispat
	local function iter()
		if (not pos) or pos > endp then return end
		local s, e = strfind(str, sep, pos, isplain)
		local part = strsub(str, pos, s and s-1)
		pos = e and e+1
		if filter then part = filter(part) end
		if part == nil then return iter() end
		return part
	end
	return iter
end

---
-- Split a string.
--
-- @param ... Same arguments as for `isplit`.
-- @return A list of strings as a table.
function M.split(str, sep, ispat, filter)
	local t, n = { }, 0
	for part in M.isplit(str, sep, ispat, filter) do
		n = n + 1
		t[n] = part
	end
	return t
end

---
-- Trim whitespace around a string.
--
-- @tparam string str  String to trim.
-- @return Trimmed string.
function M.trim(str)
	return strmatch(str, "^%s*(.-)%s*$")
end

---
-- Trim whitespace at the start of a string.
--
-- @tparam string str  String to trim.
-- @return Trimmed string.
function M.ltrim(str)
	return strmatch(str, "^%s*(.*)")
end

---
-- Trim whitespace at the end of a string.
--
-- @tparam string str  String to trim.
-- @return Trimmed string.
function M.rtrim(str)
	return strmatch(str, "(.*)%s*$")
end

---
-- Concatenate several values.
--
-- @tparam string sep  Separator between strings.
-- @tparam function func  Function expecting a single argument. It will
--  be called for each argument, and should return a string which will
--  be concatenated.
-- @tparam any ...  Arguments.
function M.xconcat(sep, func, ...)
	local n, t = select("#", ...), { ... }
	for i = 1, n do
		t[i] = func(t[i])
	end
	return table.concat(t, sep or "")
end

---
-- Concatenate several values.
--
-- Equivalent to `xconcat(sep, tostring, ...)`.
--
-- @tparam string sep  Separator between strings.
-- @tparam any ...  Arguments.
function M.concat(sep, ...)
	return M.xconcat(sep, tostring, ...)
end

---
-- Expand variable references.
--
-- This function expands variable references of the forms
-- `%VAR%`, `$VAR`, `%{VAR}`, and `${VAR}`. If `repl` is a
-- function, it is called with `VAR` as its only argument, and
-- should return a replacement string. If `repl` is a table,
-- `VAR` is used as key, and the resulting value must be a
-- string which will be used as replacement.
--
-- If the `err` function is specified it will be called on
-- invalid variable references with three values: the variable
-- name, the opening characters, and the closing characters,
-- in that order. If it returns a non-nil value, that value is
-- converted to a string and used as replacement. Otherwise,
-- invalid variable references are kept verbatim in the
-- returned string to aid in debugging.
--
-- @tparam string str  String to expand.
-- @tparam table|function repl  Replacement.
-- @tparam ?function err  Function called in case of errors.
-- @return Two values: The expanded string, the number of
--  correct replacements (not including errors).
function M.expandvars(str, repl, err)
	if type(repl) == "table" then
		local r = repl
		repl = function(k) return r[k] end
	elseif type(repl) ~= "function" then
		error("replacement must be a table or function")
	end
	local nrepl = 0
	local function replace(open, var, close)
		if ((open == "%" and close == "%")
				or (open == "$" and close == "")
				or (open == "%{" and close == "}")
				or (open == "${" and close == "}")) then
			nrepl = nrepl + 1
			local v = repl(var)
			if v ~= nil then
				return tostring(v)
			else
				return ""
			end
		elseif err then
			local v = err(var, open, close)
			if v ~= nil then
				return tostring(v)
			end
		end
		return open..var..close
	end
	return str:gsub("([%%%$]{?)([A-Za-z0-9._:-]+)([}%%]?)", replace), nrepl
end

local ok, expect = pcall(require, "expect")

if ok then -- Self-tests.

	local function replace(k)
		return "<var:"..k..">"
	end

	expect("<var:foo>", M.expandvars("$foo", replace))
	expect("<var:foo>", M.expandvars("%foo%", replace))
	expect("<var:foo>", M.expandvars("${foo}", replace))
	expect("<var:foo>", M.expandvars("%{foo}", replace))
	expect("${foo", M.expandvars("${foo", replace))
	expect("$foo}", M.expandvars("$foo}", replace))
	expect("%foo", M.expandvars("%foo", replace))
	expect("%foo+bar", M.expandvars("%foo+bar", replace))
	expect("<var:foo>+bar", M.expandvars("$foo+bar", replace))
	expect("<var:foo-bar>", M.expandvars("$foo-bar", replace))

end

return M

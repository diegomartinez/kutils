
---
-- Glob-style pattern matching.
--
-- This module implements glob-style matching for strings.
--
-- In a glob-style pattern, `*` means zero or more characters (`.*` in
-- regular expression syntax), and `?` means any character (`.?`). Any
-- other character is taken as-is. Some examples:
--
-- * `foo*` matches any string that begins in `foo`.
-- * `*foo` matches any string that ends in `foo`.
-- * `*foo*` matches any string that contains `foo` anywhere.
-- * `fo?` matches any 3-letter string that begins in `fo`.
-- * `f??k` matches any 4-letter string that begins in `f` and ends in `k`.
-- * `*a*b*` matches any string that contains an `a` and a `b`, in that
--   order, with any number of characters in-between (even none).
--
-- Note:
-- Due to metatables, calling the module results in a call to the `match`
-- function. The following two lines are equivalent:
--
--     local glob = require "glob"
--     if glob.match(s, "foo*") then --[[ ... ]] end
--     if glob(s, "foo*") then --[[ ... ]] end
--
-- @module glob

local M = { }

---
-- Converts a glob-style pattern to a Lua-style pattern.
--
-- @tparam string s  String to convert.
-- @return Converted pattern.
function M.topattern(s)
	return "^"..(s
			:gsub("[%(%)%.%[%]%+%%-]", "%%%1")  -- Escape specials
			:gsub("%*", ".*")                   -- Convert `*` to `.*`
			:gsub("%?", ".?")                   -- Convert `?` to `.?`
			).."$"
end

---
-- Matches a string against a pattern
--
-- @tparam string s  String.
-- @tparam string g  Pattern.
-- @return True if the string matches the pattern exactly,
--  false otherwise.
function M.match(s, g)
	return s:match(M.topattern(g)) ~= nil
end

local ok, expect = pcall(require, "expect")

if ok then -- Self-tests.

	expect(true,  M.match("foo", "fo*"))
	expect(false, M.match("for", "foo*"))

end

return setmetatable(M, {
	__call = function(_, ...)
		return M.match(...)
	end,
})


---
-- Type checking functions.
--
-- @module types

local M = { }

---
-- Check if a value is callable.
--
-- @tparam any x  Object to check.
-- @return True if `x` is a function or implements the `__call`
--  metamethod, false otherwise.
function M.iscallable(x)
	if type(x) == "function" then
		return true
	end
	local mt = getmetatable(x)
	return mt and M.iscallable(mt.__call) and true or false
end

---
-- Check if we can read arbitrary fields of an object.
--
-- @tparam any x  Object to check.
-- @return True if `x` is a table or implements the `__index`
--  metamethod, false otherwise.
function M.isindexreadable(x)
	local mt = getmetatable(x)
	local i = mt and mt.__index
	return ((type(i)=="function" or (i~=nil and M.isindexreadable(i)))
			or type(x)=="table") and true or false
end

---
-- Check if we can modify arbitrary fields of an object.
--
-- @tparam any x  Object to check.
-- @return True if `x` is a table or implements the `__newindex`
--  metamethod, false otherwise.
function M.isindexwritable(x)
	local mt = getmetatable(x)
	local i = mt and mt.__newindex
	return ((type(i)=="function" or (i~=nil and M.isindexwritable(i)))
			or type(x)=="table" ) and true or false
end

return M

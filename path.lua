
---
-- @module path

local M = { }

local strfind, strsub = string.find, string.sub

---
-- @tfield string dirsep
M.dirsep = package and package.config and package.config:sub(1, 1) or "/"

local function split(str, sep)
	local pos, endp = 1, #str+1
	local parts, n = { }, 0
	while pos and pos <= endp do
		local s, e = strfind(str, sep, pos, true)
		local part = strsub(str, pos, s and s-1)
		if part ~= nil then n=n+1 parts[n]=part end
		pos = e and e+1
	end
	return parts
end

local function isabs(x)
	return x:match("^[A-Za-z]:[/\\]") or x:match("^[/\\]") or nil
end

---
-- @tparam string ...
-- @treturn string fullpath
function M.collapse(...)
	local n, t = select("#", ...), { ... }
	for i = 1, n do
		t[i] = tostring(t[i])
	end
	t = table.concat(t, "/"):gsub("[/\\]+", "/")
	local abs = isabs(t) or ""
	t = t:sub(1+#abs)
	local nt, i = { }, 0
	for _, part in ipairs(split(t, "/")) do
		if part == ".." then
			if i == 0 then
				return nil, "trying to get parent of root"
			end
			nt[i] = nil
			i = i - 1
		elseif part ~= "." then
			i = i + 1
			nt[i] = part
		end
	end
	return abs..table.concat(nt, "/")
end

---
-- @tparam string ...
-- @treturn string|nil prefix
function M.isabs(...)
	return isabs(M.collapse(...))
end

---
-- @tparam string ...
-- @treturn table components
function M.split(...)
	return split(M.collapse(...), "/")
end

---
-- @tparam string ...
-- @treturn string|nil path
function M.canon(...)
	return (M.collapse(...):gsub("/", M.dirsep))
end

---
-- @tparam string ...
-- @treturn string basename
function M.basename(...)
	return M.collapse(...):match("[^/\\]*$") or ""
end

---
-- @tparam string ...
-- @treturn string dirname
function M.dirname(...)
	return M.collapse(...):match("^.*/") or ""
end

return M


---
-- Base class for exceptions.
--
-- **Extends:** `class.Object`
--
-- @classmod exceptions.Exception

local class = require "class"

local Exception = class("exceptions.Exception")

---
-- (any)
Exception.message = nil

---
-- (string)
Exception.traceback = nil

---
-- @tparam any msg
function Exception:init(msg)
	self.message = msg
	self.traceback = debug and debug.traceback and debug.traceback() or nil
end

---
-- @tparam ?boolean full
function Exception:tostring(full)
	if full then
		return (self.__name..": "..tostring(self.message)
				..(self.traceback and "\n"..self.traceback or ""))
	end
	return self.__name..": "..tostring(self.message)
end

return Exception

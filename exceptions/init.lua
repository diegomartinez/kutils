
---
-- @module exceptions

package.path = "../?.lua;../?/init.lua;"..package.path

local M = { }

local class = require "class"
local Exception = require "exceptions.Exception"

---
-- @tparam any x
-- @tparam ?number level
function M.error(x, level)
	if not class.isinstance(x, Exception) then
		x = Exception(x)
	end
	return error(x, level)
end

---
-- @tparam function func
-- @tparam function handler
-- @tparam any ...
function M.xpcall(func, handler, ...)
	local function h(x)
		if not class.isinstance(x, Exception) then
			x = Exception(x)
		end
		return handler(x)
	end
	return xpcall(func, h, ...)
end

local function source(level)
	local loc = debug and debug.getinfo and debug.getinfo(level)
	if not loc then return "[?]:0" end
	return loc.short_src
end

---
-- @tparam function func
-- @param ...
function M.main(func, ...)
	local function h(x)
		local loc = source(2)
		io.stderr:write(loc, ": ", x:tostring(true), "\n")
		os.exit(1)
	end
	M.xpcall(func, h, ...)
end

return M


---
-- Configuration file parser.
--
-- The basic syntax parsed by this module is as follows:
--
--     # This is a comment.
--     ; This is also a comment.
--
--     # The root section has no header.
--
--     # Set some variables.
--     var = value
--     var2 = value
--     var3 = value
--
--     # The following is a section header.
--     [a_section]
--
--     # Set a variable in this section.
--     var = value
--
-- Note: Thanks to metatables, calling the module is the same as
-- calling `new`. The following lines are equivalent:
--
--     local config = require "config"
--     local c = config.new()
--     local c = config()
--
-- @module config

local M = { }

local stringex = require "stringex"
local split = stringex.split
local trim  = stringex.trim
local ltrim = stringex.ltrim

---
-- Table representing a configuration file.
-- @table conf
local conf = { }
local conf_meta = { __index=conf }

---
-- Whether or not section and key names are case-sensitive.
-- (boolean, default is false).
conf.casesensitive = false

---
-- Set a string value.
--
-- @tparam string section  Section.
-- @tparam string k  Key.
-- @tparam any v  Value. If nil, key is removed from the section.
-- @return Always true.
function conf:set(section, k, v)
	assert(type(section) == "string", "section must be a string")
	assert(type(k) == "string", "key must be a string")
	local sec = self._data[k]
	if not sec then
		self._lastindex = self._lastindex + 1
		sec = {
			key = section,
			vars = { },
			index = self._lastindex,
			lastindex = 0,
		}
		self._data[k] = sec
	end
	if v == nil then
		sec[k] = nil
	else
		local var = sec.vars[k]
		if not var then
			sec.lastindex = sec.lastindex + 1
			var = { key=k, index=sec.lastindex }
			sec.vars[k] = var
		end
		var.value = tostring(v)
	end
	return true
end

---
-- Set a number value.
--
-- The passed value may either be a number, or a string that
-- `tonumber` can interpret.
--
-- @tparam string section  Section.
-- @tparam string k  Key.
-- @tparam number|string v  Value.
-- @return True on success, false if the value is not a number or a
--  string convertible to a number.
function conf:setnumber(section, k, v)
	v = tonumber(v)
	if not v then return nil, "not a valid number" end
	return self:set(section, k, v)
end

---
-- Set a boolean value.
--
-- This function takes the boolean value of `v` the same way
-- Lua does (that is, only nil and false are "false").
--
-- @tparam string section  Section.
-- @tparam string k  Key.
-- @tparam any v  Value.
-- @return Always true.
function conf:setboolean(section, k, v)
	self:set(section, k, v and "true" or "false")
end

---
-- Queries a string value.
--
-- @tparam string section  Section.
-- @tparam string k  Key.
-- @tparam any def  Default value if key does not exist.
-- @return A string if the key exists, `def` otherwise.
function conf:get(section, k, def)
	assert(type(section) == "string", "section must be a string")
	assert(type(k) == "string", "key must be a string")
	local sec = self._data[section]
	if sec then
		local v = sec.vars[k]
		if v then return v.value end
	end
	return def
end

---
-- Queries a number value.
--
-- @tparam string section  Section.
-- @tparam string k  Key.
-- @tparam any def  Default value if key does not exist.
-- @return Returns a number if the key exists and it can be converted
--  by `tonumber`, `def` otherwise.
function conf:getnumber(section, k, def)
	local v = self:get(section, k)
	return v and tonumber(v) or def
end

---
-- Queries a boolean value.
--
-- This function uses `toboolean` to convert the value. See that
-- function's documentation for details on accepted values.
--
-- @tparam string section  Section.
-- @tparam string k  Key.
-- @tparam any def  Default value if key does not exist.
-- @return Returns a boolean if the key exists and the value is
--  recognized as above, `def` otherwise.
function conf:getboolean(section, k, def)
	local v = self:get(section, k)
	if not v then return def end
	return M.toboolean(v, def)
end

local to_bool = {
	["true"]=true, t=true, y=true, yes=true,
	on=true, enable=true, enabled=true,
	["false"]=false, f=false, n=false, no=false,
	off=false, disable=false, disabled=false,
}

---
-- Utility function to convert a value to a boolean.
--
-- This function handles strings and numbers specially.
--
-- For strings, it takes any of `true`, `t`, `y`, `yes`, `on`,
-- `enable`, `enabled`, and a non-zero number-like string as "true",
-- and any of `false`, `f`, `n`, `no`, `off`, `disable`, `disabled`,
-- and zero as "false". The comparison is case-insensitive.
--
-- For numbers, returns false if it is zero, otherwise true.
--
-- Other types are converted to a boolean following the same
-- semantics of Lua (nil and false are considered "false", and any
-- other value is considered "true").
--
-- @tparam any v  Value to convert.
-- @tparam ?any def  Default value. Default is nil.
-- @return Returns a boolean the value is recognized as above,
--  `def` otherwise.
function M.toboolean(v, def)
	if type(v) == "string" then
		v = tostring(v):lower()
		local b = to_bool[v]
		if b ~= nil then
			return b
		else
			b = tonumber(v)
			if b then return b ~= 0 end
		end
		return def
	elseif type(v) == "number" then
		return v ~= 0
	end
	return v and true or false
end

local function indexsort(e1, e2)
	return e1.index < e2.index
end

---
-- Return a string representation of the config file.
function conf:tostring()
	local t, n = { }, 0
	local function add(x)
		n=n+1 t[n]=x
	end
	local sections = { }
	for _, section in pairs(self._data) do
		sections[#sections+1] = section
	end
	table.sort(sections, indexsort)
	for _, section in ipairs(sections) do
		if section.name ~= "" then
			add("["..section.name.."]")
		end
		local vars = { }
		for _, v in pairs(section.vars) do
			vars[#vars+1] = v
		end
		for _, v in ipairs(section.raw) do
			vars[#vars+1] = v
		end
		table.sort(vars, indexsort)
		for _, v in ipairs(vars) do
			if v.raw then
				add(v.raw)
			else
				add(v.key.." = "..v.value)
			end
		end
	end
	return table.concat(t, "\n")
end

conf_meta.__tostring = conf.tostring

---
-- Write the configuration to the specified file.
--
-- @tparam string filename
-- @return True on success, nil plus error message on failure.
function conf:save(filename)
	local f, e = io.open(filename, "w")
	if not f then return false, e end
	for k, v in pairs(self._vars) do
		f:write(("%s = %s\n"):format(k, v))
	end
	f:close()
	return true
end

---
-- Parse a text fragment.
--
-- This function parses the supplied data as a configuration file
-- fragment, and adds parsed data to this instance.
--
-- @tparam string data  Configuration fragment.
-- @return True on success, nil plus error message on error.
--  Warning: The configuration may be truncated or otherwise invalid
--  if this function returns nil.
function conf:parse(data)
	local sectionname = ""
	local section, vars, raw
	local function add(t)
		if not section then
			section = self._data[sectionname]
			if section then
				vars, raw = section.vars, section.raw
			else
				self._lastindex = self._lastindex + 1
				vars, raw = { }, { }
				section = {
					name = sectionname,
					index = self._lastindex,
					lastindex = 0,
					vars = vars,
					raw = raw,
				}
				self._data[sectionname] = section
			end
		end
		section.lastindex = section.lastindex + 1
		t.index = section.lastindex
		if t.raw then
			raw[#raw+1] = t
		else
			vars[t.key] = t
		end
	end
	for _, line in ipairs(split(data, "\n")) do repeat
		local k, v

		if line:match("^%s*[;#]") then
			self:oncomment(line)
			add({ raw=line })
			break -- continue
		end

		k = line:match("^%s*%[([^%[%]]+)%]%s*$")
		if k then
			self:onsection(k)
			sectionname, section = k, nil
			break -- continue
		end

		k, v = line:match("^([^=]+)=(.*)")
		if k then
			k, v = trim(k), ltrim(v)
			self:onkeyvalue(k, v)
			add({ key=k, value=v })
			break -- continue
		end

		self:oninvalid(line)
		add({ raw=line })
		break -- continue
	until false end -- luacheck: ignore
	return true
end

---
-- Called when a `[section]` line is parsed.
--
-- You may override this method in an instance to perform
-- something useful. Its default implementation does nothing.
--
-- @tparam string name  Section name.
function conf:onsection(name) -- luacheck: ignore
end

---
-- Called when a `key = value` line is parsed.
--
-- You may override this method in an instance to perform
-- something useful. Its default implementation does nothing.
--
-- @tparam string k  Key.
-- @tparam string v  Value.
function conf:onkeyvalue(k, v) -- luacheck: ignore
end

---
-- Called when a comment line is parsed.
--
-- You may override this method in an instance to perform
-- something useful. Its default implementation does nothing.
--
-- @tparam string line  Raw line. Includes comment character.
function conf:oncomment(line) -- luacheck: ignore
end

---
-- Called when an invalid line is parsed.
--
-- You may override this method in an instance to perform
-- something useful. Its default implementation does nothing.
--
-- @tparam string line  Raw line.
function conf:oninvalid(line) -- luacheck: ignore
end

---
-- Read the configuration to the specified file.
--
-- @tparam string filename
-- @return True on success, nil plus error message on failure.
function conf:load(filename)
	local f, e, d
	f, e = io.open(filename, "r")
	if not f then return nil, e end
	d, e = f:read("*a")
	f:close()
	if not d then return nil, e end
	return self:parse(d)
end

---
-- Iterate over the section names.
--
-- @return An iterator function returning section names.
function conf:isections()
	return function(_, k)
		return (next(self._data, k))
	end
end

---
-- Get the section names.
--
-- @return A list of section names as a table.
function conf:sections()
	local l, n = { }, 0
	for k in self:isections() do
		n=n+1 l[n] = k
	end
	return l
end

---
-- Iterate over the values.
--
-- @return An iterator function returning keys and values.
--  Note: If the section does not exist, the returned value will still
--  be an iterator, but it will yield no values.
function conf:ivalues(section)
	assert(type(section) == "string", "section must be a string")
	local sec = self._data[section]
	sec = sec and sec.data or { }
	return pairs(sec)
end

---
-- Get the section names.
--
-- @return A mapping of values in a section as a table.
--  Note: If the section does not exist, the returned value will still
--  be a table, but it will have no keys.
function conf:values(section)
	local t = { }
	for k, v in self:ivalues(section) do
		t[k] = v
	end
	return t
end

---
-- Create a new empty configuration object.
function M.new()
	local inst = setmetatable({ }, conf_meta)
	inst._data = { }
	inst._lastindex = 0
	return inst
end

return setmetatable(M, {
	__call = function(_, ...)
		return M.new(...)
	end,
})

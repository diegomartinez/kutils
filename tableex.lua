
---
-- Extra table utilities.
--
-- @module tableex

local M = { }

local isindexreadable

do
	local ok, lib = pcall(require, "types")
	if ok and lib and lib.isindexreadable then
		isindexreadable = lib.isindexreadable
	else
		function isindexreadable(x) -- local
			return type(x) == "table"
		end
	end
end

---
-- Get part of a table.
--
-- @tparam table t  Table to operate on.
-- @tparam ?number start  Start index. Default is 1.
-- @tparam ?number stop  End index. Default is `#t`.
-- @tparam ?number step  Step. Default is 1.
-- @return A table.
function M.slice(t, start, stop, step)
	local nt, n = { }, 0
	for i = start or 1, stop or #t, step or 1 do
		n=n+1 nt[n] = t[i]
	end
	return nt
end

---
-- Perform a shallow copy of a table.
--
-- @tparam table t  Table to copy.
-- @return A table.
function M.copy(t)
	local nt = { }
	for k, v in pairs(t) do
		nt[k] = v
	end
	return nt
-- @return A table.
end

-- Types that don't need to be copied.
local deepcopysafe = {
	string=true, boolean=true,
	number=true, ["nil"]=true,
}

-- Workhorse for deepcopy.
local function deepcopy(t, unsafe, seen)
	if seen[t] then return seen[t] end
	local nt = { }
	seen[t] = nt
	for k, v in pairs(t) do
		local tv = type(v)
		if tv == "table" then
			nt[k] = deepcopy(v, unsafe, seen)
		elseif deepcopysafe[tv] then
			nt[k] = v
		elseif unsafe then
			nt[k] = unsafe(v)
		else
			error("unsupported type: "..tv)
		end
	end
	return nt
end

---
-- Perform a deep copy of a table.
--
-- @tparam table t  Table to copy.
-- @tparam function unsafe  If specified, it must be a function expecting
--  a single argument. It is called for values we do not know how to copy.
--  It will receive the value as argument, and its return value will be
--  used as the "copy". It may also raise errors. If unspecified,
--  `deepcopy` will raise an error if an unsupported type is found.
-- @return A table.
function M.deepcopy(t, unsafe)
	return deepcopy(t, unsafe, { })
end

---
-- Get the value in a sub-table.
--
-- This function accepts a path in the form `foo.bar.baz`, and tries
-- to get field `baz`, of field `foo`, of field `bar`, of the specified
-- table. One limitation is that it cannot look up sub-fields which have
-- dots in their names. Note that whitespace around dots matters!
--
-- @tparam table t  Table.
-- @tparam string path  "Path" to the field.
-- @tparam ?any def  Default value. Default is nil.
-- @return The field's value, or `def` if the look up fails. Returns `t`
--  itself if the path is empty or only contains periods.
function M.get(t, path, def)
	for k in path:gmatch("[^.]+") do
		if not isindexreadable(t) then
			return def
		end
		t = t[k]
	end
	return t
end

local ok, expect = pcall(require, "expect")

if ok then -- Self-tests.

	local t = { a={aa={aaa=1}} }

	expect(1, M.get(t, "a.aa.aaa"))
	expect(1, M.get(t, "a.aa.....aaa"))
	expect("table", type(M.get(t, "a.aa")))
	expect("table", type(M.get(t, "a")))
	expect(true, rawequal(t, M.get(t, ".....")))

end

return M


---
-- Assert that two values are equal.
--
-- Note that this module returns the `expect` function itself, not
-- a module table!
--
-- @module expect

local function repr(x)
	return (type(x)=="string" and ("%q"):format(x) or tostring(x))
end

---
-- Assert that two values are equal.
--
-- This function compares the two values using `rawequal`. If the values
-- are equal, returns true. Otherwise it raises an error.
--
-- @function expect
-- @tparam any expected  Expected value.
-- @tparam any got  Value to check.
-- @return True on success, raises an error on failure.
local function expect(expected, got)
	return (rawequal(expected, got) or
			error("assertion failed"
				..": expected "..repr(expected)
				..", got "..repr(got), 2))
end

-- Self-tests, and example usage.

expect(true, 1==1)
expect(false, false)
expect(nil) -- implicit nil for `got`
expect()    -- implicit nil for both args
expect(true, expect(true, 1==1))
expect(false, pcall(expect, true, 1==0))

return expect
